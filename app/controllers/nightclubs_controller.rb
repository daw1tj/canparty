class NightclubsController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :set_nightclub, only: [:show, :edit, :update, :destroy]

  # GET /nightclubs
  # GET /nightclubs.json
  def index
    @nightclubs = Nightclub.all
  end

  # GET /nightclubs/1
  # GET /nightclubs/1.json
  def show
    @gallery_and_photo = [Gallery.new, Photo.new]
  end

  # GET /nightclubs/new
  def new
    @nightclub = Nightclub.new
  end

  def adapted
    @adaptedclubs = Nightclub.adapted
  end


  # GET /nightclubs/1/edit
  def edit
  end

  # POST /nightclubs
  # POST /nightclubs.json
  def create
    @nightclub = Nightclub.new(nightclub_params)

    respond_to do |format|
      if @nightclub.save
        format.html { redirect_to @nightclub, notice: 'Local creado exitosamente, ' }
        format.json { render :show, status: :created, location: @nightclub }
      else
        format.html { render :new }
        format.json { render json: @nightclub.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /nightclubs/1
  # PATCH/PUT /nightclubs/1.json
  def update
    respond_to do |format|
      if @nightclub.update(nightclub_params)
        format.html { redirect_to @nightclub, notice: 'Local actualizado exitosamente, ' }
        format.json { render :show, status: :ok, location: @nightclub }
      else
        format.html { render :edit }
        format.json { render json: @nightclub.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nightclubs/1
  # DELETE /nightclubs/1.json
  def destroy
    if @nightclub.user != current_user
      @nightclub.destroy
      respond_to do |format|
        format.html { redirect_to my_nightclubs_path, notice: 'Local destruido exitosamente, ' }
        format.json { head :no_content }
      end
    else
      redirect_to my_nightclubs_path, notice: 'No tienes permisos para destruir el local, '
    end
  end

  # GET /my_nightclubs
  def my_nightclubs
    @nightclubs = Nightclub.where(user_id: current_user.id)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nightclub
      @nightclub = Nightclub.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nightclub_params
      params.require(:nightclub).permit(:user_id, :name, :price, :location, :description, :date,
                                        :validity, :adapted, :image, :latitude, :longitude)
    end
end
