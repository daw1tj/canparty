class Gallery < ApplicationRecord
  belongs_to :nightclub, required: true
  has_many :photos
end
