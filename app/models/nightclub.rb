class Nightclub < ApplicationRecord
  belongs_to :user #representante del local en la página
  has_many :galleries

  has_attached_file :image, styles: { small: "64x64", med: "100x100", large: "200x200", thumbnail: "171x180"}
  validates_attachment_file_name :image, matches: [/png\z/, /jpe?g\z/]

  scope :adapted, -> { where(adapted: true) }
  scope :no_adapted, -> { where(adapted: false) }
end
