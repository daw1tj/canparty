class Photo < ApplicationRecord
  belongs_to :gallery, required: true
  has_attached_file :image, styles: { small: "64x64", med: "100x100", large: "200x200", thumbnail: "171x180"}
  validates_attachment_file_name :image, matches: [/png\z/, /jpe?g\z/]

end
