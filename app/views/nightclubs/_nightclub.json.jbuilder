json.extract! nightclub, :id, :name, :price, :location, :date, :validity, :adapted, :created_at, :updated_at
json.url nightclub_url(nightclub, format: :json)