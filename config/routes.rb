Rails.application.routes.draw do
  devise_for :users
  resources :photos
  resources :galleries
  resources :nightclubs do
    resources :galleries
  end
  get '/my_nightclubs' => 'nightclubs#my_nightclubs'
  resources :questions
  resources :home
  get '/adapted_nightclubs' => 'nightclubs#adapted', :as => :adapted
  match '/home' => 'home#index', via: [:get, :post]
  get 'events' => 'home#events'
  get '/photography' => 'photography#index'

  root 'home#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
