class CreateNightclubs < ActiveRecord::Migration[5.0]
  def change
    create_table :nightclubs do |t|
      t.string :name
      t.float :price
      t.string :location
      t.date :date
      t.date :validity
      t.boolean :adapted

      t.timestamps
    end
  end
end
