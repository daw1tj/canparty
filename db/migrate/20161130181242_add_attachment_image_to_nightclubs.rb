class AddAttachmentImageToNightclubs < ActiveRecord::Migration
  def self.up
    change_table :nightclubs do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :nightclubs, :image
  end
end
