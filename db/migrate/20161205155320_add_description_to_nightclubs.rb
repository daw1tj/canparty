class AddDescriptionToNightclubs < ActiveRecord::Migration[5.0]
  def change
    add_column :nightclubs, :description, :text
  end
end
