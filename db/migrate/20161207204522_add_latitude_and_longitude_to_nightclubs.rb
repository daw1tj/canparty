class AddLatitudeAndLongitudeToNightclubs < ActiveRecord::Migration[5.0]
  def change
    add_column :nightclubs, :latitude, :float
    add_column :nightclubs, :longitude, :float
  end
end
