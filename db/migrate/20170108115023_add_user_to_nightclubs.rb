class AddUserToNightclubs < ActiveRecord::Migration[5.0]
  def change
    add_reference :nightclubs, :user, foreign_key: true
  end
end
