require 'test_helper'

class NightclubsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @nightclub = nightclubs(:one)
  end

  test "should get index" do
    get nightclubs_url
    assert_response :success
  end

  test "should get new" do
    get new_nightclub_url
    assert_response :success
  end

  test "should create nightclub" do
    assert_difference('Nightclub.count') do
      post nightclubs_url, params: { nightclub: { adapted: @nightclub.adapted, date: @nightclub.date, location: @nightclub.location, name: @nightclub.name, price: @nightclub.price, validity: @nightclub.validity } }
    end

    assert_redirected_to nightclub_url(Nightclub.last)
  end

  test "should show nightclub" do
    get nightclub_url(@nightclub)
    assert_response :success
  end

  test "should get edit" do
    get edit_nightclub_url(@nightclub)
    assert_response :success
  end

  test "should update nightclub" do
    patch nightclub_url(@nightclub), params: { nightclub: { adapted: @nightclub.adapted, date: @nightclub.date, location: @nightclub.location, name: @nightclub.name, price: @nightclub.price, validity: @nightclub.validity } }
    assert_redirected_to nightclub_url(@nightclub)
  end

  test "should destroy nightclub" do
    assert_difference('Nightclub.count', -1) do
      delete nightclub_url(@nightclub)
    end

    assert_redirected_to nightclubs_url
  end
end
